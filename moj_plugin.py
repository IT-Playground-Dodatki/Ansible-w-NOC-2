from __future__ import (absolute_import, division, print_function)
from ansible.plugins.callback import CallbackBase
from pprint import pprint

class CallbackModule(CallbackBase):

    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'moj_plugin'

    def __init__(self, display=None):
        self.dane_wyjsciowe = {}
        super(CallbackModule, self).__init__(display=display)

    def v2_playbook_on_start(self, playbook):
        self.dane_wyjsciowe['nazwa-pliku'] = playbook._file_name
        super(CallbackModule, self).v2_playbook_on_start(self)

    def v2_playbook_on_stats(self, stats):
        pprint(“Nazwa pliku playbooka: “ + self.dane_wyjsciowe['nazwa-pliku'])
        super(CallbackModule, self).v2_playbook_on_stats(self)
        