# Suplement do artykułu "Ansible w służbie NOC (część 2)" z numeru 08/2018 miesięcznika "IT Professional"

Repozytorium zawiera skrypty, które były omawiane w moim artykule na łamach miesięcznika "IT Professional". 

### dziedziczenie.py

Programowanie obiektowe, nie tylko w języku Python, wymaga poznania zasad dziedziczenia właściwości klas. Powyższy plik zawiera bardzo proste i zrozumiałe demo, które pozwoli zrozumieć zasadę dziedziczenia, wywoływania metod i ich nadpisywania

### moj_plugin.py

Kod przykładowej wtyczki (plugin) typu CallBack dla projektu w Ansible

