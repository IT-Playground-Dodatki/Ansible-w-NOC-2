from __future__ import (absolute_import, division, print_function)

class Rodzic(object):
    def __init__(self):
        self.imie = "Jan"
        self.nazwisko = "Kowalski"

    def napisz_imie(self):
        print("Rodzic: Imie  - ", self.imie)

    def napisz_nazwisko(self):
        print("Rodzic: Nazwisko  - ", self.nazwisko)

    def zwroc_nazwisko(self):
        return self.nazwisko


class Dziecko(Rodzic):
    def __init__(self):
        super(Dziecko, self).__init__()
        self.imie = "Tadeusz"

    def napisz_imie(self):
        print("Dziecko: Imie  - ", self.imie)

    def napisz_nazwisko_rodzica(self):
        print("Dziecko: Nazwisko rodzica  - ", super(Dziecko, self).zwroc_nazwisko())


class Wnuk(Dziecko):
    def __init__(self):
        super(Wnuk, self).__init__()
        print("Wnuk (konstruktor) Imie rodzica  - ", self.imie)
        self.imie = "Tomasz"


R = Rodzic()
R.napisz_imie()
R.napisz_nazwisko()

D = Dziecko()
D.napisz_imie()
D.napisz_nazwisko_rodzica()

W = Wnuk()
print("Wnuk: Imie - ", W.imie)
print("Wnuk: Nazwisko - ", W.nazwisko)


